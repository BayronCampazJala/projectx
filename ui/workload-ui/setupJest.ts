/* eslint-disable import/no-extraneous-dependencies */
import 'jest-preset-angular/setup-jest'

import type { Config } from '@jest/types'

const config: Config.InitialOptions = {
  verbose: true,
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
}

export default config
