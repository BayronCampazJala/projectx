import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HomeComponent } from 'src/app/pages/home/home.component'

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HomeComponent,
  ],
  providers: [],
})
export class HomeModule { }
