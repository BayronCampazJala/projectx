module.exports = {
  preset: "jest-preset-angular",
  verbose: true,
  roots: [
    "<rootDir>/src"
  ],
  moduleNameMapper: {
    "@app/(.*)$": "<rootDir>/src/app/$1",
    "@env/(.*)": "<rootDir>/src/environments/environment"
  },
  setupFilesAfterEnv: [
    "<rootDir>/setupJest.ts"
  ],
  testPathIgnorePatterns: [
    "<rootDir>/node_modules/",
    "<rootDir>/dist/",
    "<rootDir>/src/test.ts"
  ],
  globals: {
    "ts-jest": {
      "tsconfig": "<rootDir>/tsconfig.spec.json",
      "stringifyContentPathRegex": "\\.html$"
    }
  },
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 60,
      statements: -10
    }
  },
  coveragePathIgnorePatterns: [
    "\.mock\.ts$",
    "\.component\.ts$",
  ]
}
