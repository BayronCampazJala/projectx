﻿using Bureau.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Bureau.Api.Controllers
{
    [ApiController]
    [Route("api/v1/health")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public ActionResult<Health> Get()
        {
            return this.Ok(new Health { Status = "healthy" });
        }
    }
}
